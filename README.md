# ttf-asap

Contemporary sans-serif typeface family from Omnibus-Type

https://www.omnibus-type.com/fonts/asap/

https://github.com/Omnibus-Type/Asap

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/fonts/ttf-asap.git
```
